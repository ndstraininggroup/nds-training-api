import { ITask } from './ITask';

export interface ITaskDb {
  tasks: ITask[];
}
