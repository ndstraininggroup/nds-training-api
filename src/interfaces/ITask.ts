export interface ITask {
  id: number;
  title: string;
  desc: string;
  status: 'todo' | 'started' | 'complete' | string;
  priority: 'high' | 'medium' | 'low' | string;
}
