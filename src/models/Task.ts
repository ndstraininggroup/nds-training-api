import { ITask } from '../interfaces/ITask';

export class Task implements ITask {
  constructor(data: ITask) {
    data.id = this.id;
    data.title = this.title;
    data.desc = this.desc;
    data.status = this.status;
    data.priority = this.priority;
  }

  id: number;
  title: string;
  desc: string;
  status: string;
  priority: string;
}
