import * as fs from 'fs';
import * as path from 'path';
import { ITaskDb } from '../interfaces/ITasks';
import { ITask } from '../interfaces/ITask';

export class TaskController {
  /**
   * Get all Tasks
   *
   * @param req Request
   * @param res Response
   */
  getTasks(req, res) {
    return res.json(this.allTasks());
  }

  /**
   * Get a task by ID
   *
   * @param req Request
   * @param res Response
   */
  getTask(req, res) {
    const tasks: ITask[] = this.allTasks();
    const taskId: number = Number(req.params.taskId);

    // Ensure that the taskId was passed in via the request params, exists and is numeric
    if (isNaN(taskId)) {
      // The taskId is NaN so return an error
      return res.json({
        status: 'Failed',
        message: `Expected 'taskId' to be numeric but received ${taskId}`,
        error: 'Invalid Input',
      });
    } else {
      // Find the Task by ID
      const task: ITask = tasks.find(task => {
        return task.id === taskId;
      });

      // If a Task was found return it
      if (task) {
        return res.json(task);
      }
    }

    // If there was not a taskId then return all tasks
    return res.json({
      status: 'Success',
      message: `Could not find a Task where id = ${taskId}`,
      error: null,
    });
  }

  /**
   * Add a new Task
   *
   * @param req Request
   * @param res Response
   */
  public post(req, res) {
    // TODO: Implement POST so a new Task can be added (Need a real DB first!)

    if (req.body) {
      const task: ITask = req.body;

      ///
      /// Validate and insert the new task into the DB ... if there was one
      ///

      return res.json(JSON.stringify(task));
    }

    res.end(`Sorry, no data was passed in. No new task was be created.`);
    return res.json({
      status: 'Failure',
      message: `Sorry, no data was passed in. No new task was be created.`,
      error: 'Invalid or missing input',
    });
  }

  /**
   * Pull all the Tasks out of the JSON file and return an array of ITask
   *
   * @returns ITask[]
   */
  private allTasks(): ITask[] {
    const tasksDbFile = path.resolve(__dirname, '../db/tasks.db.json');
    const tasksDB: ITaskDb = JSON.parse(
      fs.readFileSync(tasksDbFile).toString('utf8'),
    );

    return tasksDB.tasks;
  }
}
