import * as chalk from 'chalk';
import * as cors from 'cors';
import * as express from 'express';
import { TaskController } from './controllers/taskController';

const app = express();
const router = express.Router();
const port = process.env.PORT || 3000;

/********************
 * Apply Middleware *
 ********************/
app.use(cors()).use('/task', router);

/*******************************
 * API Routes For Path `/task` *
 *******************************/
router.get('/', (req, res) => {
  return new TaskController().getTasks(req, res);
});

router.get('/:taskId', (req, res) => {
  return new TaskController().getTask(req, res);
});

router.post('/', (req, res) => {
  return new TaskController().post(req, res);
});

//#region 'Original basic routes'

app.get('/', (req, res) => {
  res.send(`Welcome Home!  This is your main entry point.`);
});

app.get('/api', (req, res) => {
  res.json({
    name: '<Your Name Here>',
    team: '<Your Team Name Here>',
    cpuUsage: process.cpuUsage(),
    memoryUsage: process.memoryUsage(),
    hostname: req.hostname,
    path: req.path,
    method: req.method,
  });
});

//#endregion

/**********************************************
 * Listen for incoming connections to the API *
 **********************************************/
app.listen(port, () => {
  console.log(`${chalk.blue(`Express is listening on port ${port}...`)}`);
});
