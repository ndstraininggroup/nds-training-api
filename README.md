# NDS Training App

## Summary

A simple API using Node.js, Express, and Typescript

### Getting Started

1. Open up a new bash terminal and perform the following

   - Clone the repository using SSH (if you have cloned this already, you won't need to again)

   ```bash
   git clone https://bitbucket.org/ndstraininggroup/nds-training-api.git
   ```

   - Change directory into the repository (simple DOS commands will work to navigate around)

   ```bash
   cd nds-training-api
   ```

2. In the terminal, ensure you are in the `nds-training-api` directory
3. Install the NPM packages (from within the project directory) just take the defaults when prompt

   ```bash
   npm i
   ```

### Running the project

1. In a new bash terminal, ensure you are in the nds-training-api directory and then start the application by entering the following command in a the bash terminal

   ```bash
   npm start
   ```

2. See the project running by opening the browser (or Postman) and going to `http://localhost:3030`
